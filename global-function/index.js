import { xpath, variables } from "../global-variables"

export function login() {
    cy.visit('/');
    cy.title().should('eq', 'MyOrder')
    cy.xpath(xpath.xpathEmail).type(variables.email)
    cy.xpath(xpath.xpathPassword).type(variables.password)
    cy.wait(1000)
    cy.xpath(xpath.xpathButtonLogin).click()
    cy.wait(3000)
    cy.xpath(xpath.xpathSelectTeam).click()
    cy.title().should('eq', 'MyOrder')
}